﻿$(function () {
    /*jshint esversion: 6 */
    "use strict";

    /**
    * Reloads the window to reset all functions and variables to its original state
    */
    function reloadWindow() {
        window.location.reload();
    }
    $("#restartGame button").click(reloadWindow);

    var selectedDicesArr = [];
    var resetArr = [];

    var pointsDict = {};

    var bonusPoints = 63;
    var playCount = 13;
    var diceRolls = 0;



    /**
    * The Table class is used for calculations and dice shuffling
    * @param {number} selectedDicesArr
    */
    function Table() {

        /**
         * Calculates the total score and fills the #totalScore cell. 
         * It uses the parsed in points to do so.
         * 
         * @param {number} point is the score  parsed in through the parent function.
         */
        this.calculateTotalScore = function () {
            var totalScore = 0;
            pointsDict['totalScore'] = 0;

            for (var point in pointsDict) {
                totalScore += parseInt(pointsDict[point]);
            }

            $("#totalScore").text(totalScore);
            $("#totalScore").css("color", "orange");
            pointsDict['totalScore'] = totalScore;
            this.fillTable();
        }

        /**
        * Used for making the proper calculations. It is error prone where a selected dice
        * could not be of the same value. So the function does not take it into consideration
        * using the 'val' paramater as reference. If the user clicks on anything that is not 
        * the correct cell, it puts a zero instead as the score.
        * 
        * @param {string} selectedID is the id used to where to put the correct points.
        * @param {number} val is the value to be used as reference.
        */
        this.setSinglePoints = function (selectedID, val) {

            var pointsArr = selectedDicesArr.filter(element => parseInt(element) == val);
            var totalPoints = 0;

            // Checks that the cell is empty and that the selection is correct 
            //  before putting data
            if (this.isCellEmpty(`${selectedID}`)) {

                if (pointsArr.length > 0) {
                    totalPoints = pointsArr.reduce((p1, p2) => p1 + p2);
                    if (this.isCellEmpty(`${selectedID}`)) {
                        pointsDict[`${selectedID}`] = totalPoints;
                        currentTopSum += totalPoints;
                        bonusPoints -= totalPoints;
                    }
                    $(`#${selectedID}`).text(totalPoints);
                    $(`#${selectedID}`).css("color", "orange");
                } else {
                    pointsDict[`${selectedID}`] = totalPoints;
                    $(`#${selectedID}`).text(totalPoints);
                    $(`#${selectedID}`).css("color", "orange");
                }

                $("#bonus").text(bonusPoints);
                $("#bonus").css("color", "red"); // shows in red as it is not set yet
                $('#sum').text(currentTopSum);
            }

            this.calculateTotalScore();
        }

        /**
        * Used to keep track of the points
        */
        this.fillTable = function () {
            // Fills up the table according to the values in the dictionary
            for (var el in pointsDict) {
                $(`#${el}`).text(pointsDict[el]);
                $(`#${el}`).css("color", "orange");
            }

            if (playCount <= 0) {
                alert("GAME ENDED\nPOINTS: " + $("#totalScore").text());
            } else {
                playCount--;
                $("#rollsCount span").text(playCount);
                diceRolls = 0;

                $("#player button").removeAttr("disabled", false);
                $("#player button").css("background-color", "var(--bckgColor)");
                $("#player button").css("color", "whiteSmoke");
                $("#player button").css("cursor", "pointer");

                $("#player button:hover").css("background-color", "goldrenrod");
                $("#player button:hover").css("color", "var(--bckgColor)");
            }

            this.resetBoard();
        }

        /**
        * Resets the board and moves all the dices back into the #rolledDices div.
        * Once this is done, rollDices is called to remove the previous roll.
        */
        this.resetBoard = function () {
            for (var i = 0; i < 5; ++i) {

                $("#rolledDices").append(resetArr[i]);
            }

            // Emptying the array for new elements
            selectedDicesArr = [];
            resetArr = [];

            this.rollDices();
        }

    }
    /* END OF TABLE CLASS */

    var table = new Table();

    Table.prototype.isCellEmpty = function (currentCell) {

        console.log("Is cell empty: " + ($("#" + currentCell).html() == ""));
        return ($("#" + currentCell).html() == "");
    }

    /**
        * Disables the ROLL button until the points are scored in the table 
        * through the fillTable function.
        * 
        * @param {number} rolls is the amount of rolls that have already occured
        */

    Table.prototype.checkRoll = function (rolls) {
        // Checks for the amount of plays that can take place.
        if (rolls == 3) {
            $("#player button").attr("disabled", true);
            $("#player button").css("background-color", "gray");
            $("#player button").css("color", "black");
            $("#player button").css("cursor", "none");

        }
    }

    /**
    * Dice rolls. If the parent is "rolledDices" then the dice is rolled otherwise, it doesn't.
    */
    Table.prototype.rollDices = function () {

        const dices = [
            "res/img/dice1.png",
            "res/img/dice2.png",
            "res/img/dice3.png",
            "res/img/dice4.png",
            "res/img/dice5.png",
            "res/img/dice6.png"
        ];

        var dicesToAnimate = $("#rolledDices img[id^='dice']");
        dicesToAnimate.animate({ opacity: '0' }, "slow");

        for (var i = 0; i < 5; ++i) {
            let diceRoll = Math.floor(Math.random() * 6);
            if ($(`#dice${i + 1}`).parent().attr("id") === "rolledDices") {
                $(`#dice${i + 1}`).attr("src", dices[diceRoll]);
            }
        }

        dicesToAnimate.animate({ opacity: '1' }, "slow");

        diceRolls += 1;
        table.checkRoll(diceRolls);

    }
    $("#player button").click(table.rollDices);



    /**
    * Hightights the selected dices in and adds/removes to the selectedDices array 
    */
    Table.prototype.selectDices = function () {

        let parentID = $(this).parent().attr("id");
        let currentDice = $(this).attr("src").slice(-5, -4);


        if (parentID === "rolledDices") {
            $("#selectedDices").append($(this));

            selectedDicesArr.push(Number(currentDice));
            resetArr.push($(this));

        } else {
            $("#rolledDices").append($(this));
            selectedDicesArr.splice(selectedDicesArr.indexOf(currentDice), 1);
            resetArr.splice(resetArr.indexOf($(this), 1));
        }
    }
    $("img").click(table.selectDices);


    /**
    * Switch case function to determine the proper calculatation using an OOP approach using the 
    * setSinglePoints() function which takes the id and the number to use as refence.
    */
    var currentTopSum = 0;
    Table.prototype.calculateSinglePoints = function () {
        let currentID = $(this).attr("id");

        switch (currentID) {
            case "ones":
                table.setSinglePoints("ones", 1);
                break;

            case "twos":
                table.setSinglePoints("twos", 2);
                break;

            case "threes":
                table.setSinglePoints("threes", 3);
                break;

            case "fours":
                table.setSinglePoints("fours", 4);
                break;

            case "fives":
                table.setSinglePoints("fives", 5);
                break;

            case "sixes":
                table.setSinglePoints("sixes", 6);
                break;
        }



    }
    $(".singles").click(table.calculateSinglePoints);

    /**
    * Same kind score whether three or four of same kind dices. It checks the length of sortedArr to 
    * determine if there is enough selected dices using the selected cell. If that is not the case, a
    * 0 is put in its place.
    */
    Table.prototype.setSameKind = function () {
        let cellTypeID = $(this).attr("id");
        let value = (cellTypeID == "threeOfAKind") ? 3 : 4;
        let valueTotake = parseInt(selectedDicesArr[2]); // The program breaks if the array is not long enough
        let sortedArr = selectedDicesArr.sort();
        let filteredArr = selectedDicesArr.filter((el) => parseInt(el) == valueTotake);
        let totalPoints = filteredArr.reduce((p1, p2) => p1 + p2);
        
        if (table.isCellEmpty(cellTypeID) === true) {

            if (sortedArr.length === value) {

                pointsDict[`${cellTypeID}`] = totalPoints;
                $(`#${cellTypeID}`).text(totalPoints);

            } else {
                totalPoints = 0;
                pointsDict[`${cellTypeID}`] = totalPoints;
                $(`#${cellTypeID}`).text(totalPoints);
            }

            $(`#${cellTypeID}`).css("color", "orange");
        } else {
            alert("Wrong cell selected");
        }


        table.calculateTotalScore();
    }
    $(".sameKind").click(table.setSameKind);

    /**
    * Sets the score for the small or large straight
    */
    Table.prototype.setSmallOrLargeStraight = function () {
        let straightTypeID = $(this).attr("id");
        const straightArr = [1, 2, 3, 4, 5, 6];
        let totalPoints = 0;
        let sortedArr = selectedDicesArr.sort();
        let isStraightType = sortedArr.every(el => straightArr.includes(el));

        if (table.isCellEmpty(straightTypeID)) {

            if (isStraightType && sortedArr.length >= 3) {

                if (straightTypeID === "smallStraight") {
                    totalPoints = 30;
                    $(`#${straightTypeID}`).text(totalPoints);
                    pointsDict[`${straightTypeID}`] = totalPoints;

                } else if (straightTypeID === "largeStraight") {
                    totalPoints = 40;
                    $(`#${straightTypeID}`).text(totalPoints);
                    pointsDict[`${straightTypeID}`] = totalPoints;
                }

            } else {
                $(`#${straightTypeID}`).text(0);
                pointsDict[`${straightTypeID}`] = 0;
            }
        }

        table.calculateTotalScore();
    }
    $(".straightType").click(table.setSmallOrLargeStraight);


    /**
    * Sets the fullHouse score in the table cell. part1Arr and part2Arr are used to break the received 'selectedDicesArr'
    * into two parts.
    * 
    * The if statement checks if the upper half, i.e. part1Arr, is bigger than the lower part, i.e. part2Arr.
    * Based on that it checks the sequence and if it's a match or not. If that is the case
    * the points are added else, nothing happens and the board is reset.
    */
    Table.prototype.setFullHouse = function () {
        let sortedArr = selectedDicesArr.sort();
        let part1Arr = (sortedArr[1] === sortedArr[2]) ? sortedArr.slice(0, 3) : sortedArr.slice(0, 2);
        let part2Arr = (sortedArr[2] === sortedArr[3]) ? sortedArr.slice(2, 5) : sortedArr.slice(3, 5);
        let totalPoints = 0;

        if (table.isCellEmpty($(this).attr("id"))) {

            if (part1Arr.length > part2Arr.length) {
                if (part1Arr[0] === part1Arr[1] && part1Arr[1] === part1Arr[2] &&
                    part2Arr[0] === part2Arr[1]) {
                    totalPoints = 25;
                    $("#fullHouse").text(totalPoints);
                    pointsDict["fullHouse"] = totalPoints;
                }
            } else {
                if (part1Arr[0] === part1Arr[1] &&
                    part2Arr[0] === part2Arr[1] && part2Arr[1] === part2Arr[2]) {
                    totalPoints = 25;
                    $("#fullHouse").text(totalPoints);
                    pointsDict["fullHouse"] = totalPoints;
                }
            }
        }

        table.calculateTotalScore();
    }
    $("#fullHouse").click(table.setFullHouse);


    /**
    * Set the Yahtzee points in the #yahtzee  cell. It takes in the selectedDicesArr at position 0
    * as reference to filter out any elements in the array that are not the same. Then all the points 
    * in the array are added to be used in case the #yahtzee cell is already taken. 
    * 
    * VARIABLES:
    *  currentID:     refers to the id that is clicked.
    *  currentClass:  refers to the class category of points to add.
    */
    Table.prototype.setYahtzee = function () {
        let currentID = $(this).attr("id");
        let currentClass = $(this).attr("class");
        const valueToTake = parseInt(selectedDicesArr[0]);
        let filteredArr = selectedDicesArr.filter((el) => parseInt(el) === valueToTake);
        let currentTotalPoints = filteredArr.reduce((p1, p2) => p1 + p2);
        let totalPoints = 0;

        if (table.isCellEmpty(currentID)) {

            if (filteredArr.length == 5 && currentID === "yahtzee") {

                totalPoints = 50;
                $("#yahtzee").text(totalPoints);
                pointsDict[`${currentID}`] = totalPoints;

            } else if (currentID !== "yahtzee" && filteredArr.length == 5
                && currentClass === "singles") {

                totalPoints = 100 + currentTotalPoints;
                $(`#${currentID}`).text(totalPoints);
                pointsDict[`${currentID}`] = totalPoints;

            } else {
                totalPoints = currentTotalPoints;
                $(`#${currentID}`).text(totalPoints);
                pointsDict[`${currentID}`] = totalPoints;
            }

        }
        table.calculateTotalScore();
    }
    $("#yahtzee").click(table.setYahtzee);


    /**
    * Set the Chance points in the #chance cell using selectedDicesArr to add all the points together.
    * Only the points in the 'Selected DICES' section are taken into account. 
    */
    Table.prototype.setChanceScore = function () {
        let totalPoints = selectedDicesArr.reduce((p1, p2) => parseInt(p1) + parseInt(p2));

        if (table.isCellEmpty($(this).attr("id"))) {

            $("#chance").text(totalPoints);
            pointsDict["chance"] = parseInt(totalPoints);
        }

        table.calculateTotalScore();
    }
    $("#chance").click(table.setChanceScore)


    /**
    * Calculate if the upper section is more than 63 points otherwise it generates an alert warning
    * that the points cannot be set.
    * */
    Table.prototype.setBonusPoint = function () {
        let totalPoints = 0;
        if (table.isCellEmpty($(this).attr("id"))) {

            if (bonusPoints >= 63) {
                totalPoints = 35;
                $("#bonus").text(totalPoints);
                $("#bonus").css("color", "orange");
                points["bonus"] = totalPoints;
                table.calculateTotalScore();
            } else {
                alert("Not enough top points to set score.");
            }
        }
    }
    $("#bonus").click(table.setBonusPoint);

});